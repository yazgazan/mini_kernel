
NAME=		kernel

CC=		gcc
CFLAGS=		-m32
LDFLAGS=	

ASM=		nasm
ASM_FORMAT=	-f elf32

LINK=		ld
LINK_ARCH=	elf_i386
LINK_FILE=	link.ld

RM=		rm -vf

QEMU=		qemu-system-i386
QEMU_OPTS=	-no-reboot

SRCS=		$(shell find . -name "*.asm" -or -name "*.c")
OBJS=		$(addsuffix .o, $(SRCS))

%.asm.o:	%.asm
	$(ASM) $(ASM_FORMAT) $< -o $@

%.c.o:		%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(NAME):	$(OBJS)
	$(LINK) -m $(LINK_ARCH) -T $(LINK_FILE) -o $(NAME) $(OBJS)

all:		$(NAME)

clean:
	$(RM) $(OBJS)

fclean:		clean
	$(RM) $(NAME)

re:	fclean all

run:	all
	$(QEMU) $(QEMU_OPTS) -kernel $(NAME)

.PHONY:		all clean fclean re

